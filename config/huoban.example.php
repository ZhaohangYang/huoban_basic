
<?php
return [
    'huoban_pass' => [
        // 应用信息
        'application_id'     => '',
        'application_secret' => '',
        // 配置名称
        'name'               => 'huoban_pass',
        // 是否启用别名模式
        'alias_model'        => true,
        // 权限类别  enterprise/table
        'app_type'           => 'enterprise',
        // 工作区id
        'space_id'           => '4000000002891045',
        // api地址
        'api_url'            => 'https://api.huoban.com',
        // 上传文件地址
        'upload_url'         => 'https://upload.huoban.com',
        // 缓存文件存放地址
        'cache_path'         => __DIR__ . '/../storage_path/huoban/',
    ],
];