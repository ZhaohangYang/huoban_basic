<?php

namespace App\ModelsHuayi;

use App\ModelsHuayi\HuayiBasic;

class HuayiApi extends HuayiBasic
{
    public function getMachines($machine_id = null)
    {
        $params = [];
        if ($machine_id) {
            $params = ['machine_id' => $machine_id];
        }
        $huayi_data = $this->execute('GET', 'get_machines', $params);

        return $huayi_data;
    }

    public function getConsumers($client_id = null)
    {
        $params = [];
        if ($client_id) {
            $params = ['client_id' => $client_id];
        }
        $huayi_data = $this->execute('GET', 'get_consumers', $params);

        return $huayi_data;
    }

    public function getStockEvent($machine_id, $from_date, $to_date)
    {
        $params = [
            'machine_id' => $machine_id,
            'from_date'  => $from_date,
            'to_date'    => $to_date,
        ];
        $huayi_data = $this->execute('GET', 'get_stock_event', $params);

        return $huayi_data;
    }

    public function get_machine_status($machine_id)
    {
        $params = [
            'machine_id' => $machine_id,
        ];
        $huayi_data = $this->execute('GET', 'get_machine_status', $params);

        return $huayi_data;
    }
}
