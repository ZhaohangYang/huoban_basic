<?php

namespace App\Http\Controller;

use Illuminate\Container\Container;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class IndexController implements RequestHandlerInterface
{

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->config    = $this->container->get('config');
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        try {
            $routeParams = $request->getAttribute('routeParams');
            $action      = $routeParams['action'];

            if (!method_exists($this, $action)) {
                throw new \Exception("not find", 1);
            }

            $params = $request->getParsedBody();
            $this->$action($params);

            $response = ['code' => '0', 'message' => '伙伴中间件已接收到你的请求', 'success' => true];
        } catch (\Throwable $th) {
            $response = ['code' => '500', 'message' => $th->getMessage(), 'success' => false];
        }

        return new JsonResponse($response);
    }

}
