<?php
namespace App\Models\Db;

use Illuminate\Database\Capsule\Manager as Capsule;

class DbBasic
{
    public $capsule;

    public function __construct()
    {
        $this->capsule = new Capsule;
        $this->capsule->addConnection([
            'driver'      => 'mysql',
            'host'        => 'localhost',
            'database'    => 'test',
            'username'    => 'test',
            'password'    => '123',
            'charset'     => 'utf8',
            'collat​​ion' => 'utf8_unicode_ci',
            'prefix'      => '',
        ]);
        // 设置全局静态可访问DB
        $this->capsule->setAsGlobal();
    }

}
