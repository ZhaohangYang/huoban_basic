<?php
/*
 * @Author: ZhaohangYang <yangzhaohang@comsenz-service.com>
 * @Date: 2021-06-23 16:58:47
 * @Description: 伙伴智慧大客户研发部
 */
namespace App\ModelsHuoban;

use App\ModelsHuoban\HuobanActionMapping;

class HuobanExample extends HuobanActionMapping
{
    public static $tableAlias    = '';
    public static $preTableAlias = '';

    public static function initHuobanMapping()
    {
        static::mappingBasic(['mappingXxToItemid']);
    }

    public static function mappingXxToItemid($response)
    {

    }
}
