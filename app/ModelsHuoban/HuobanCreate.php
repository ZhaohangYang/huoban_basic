<?php
/*
 * @Author: ZhaohangYang <yangzhaohang@comsenz-service.com>
 * @Date: 2021-06-23 16:58:47
 * @Description: 伙伴智慧大客户研发部
 */
namespace App\ModelsHuoban;

use Huoban\Huoban;
use Illuminate\Support\Facades\Cache;

class HuobanCreate
{
    public static function create($huoban_config)
    {
        $ticket_name = $huoban_config['name'] . $huoban_config['app_type'] . 'ticket';
        if (!Cache::has($ticket_name)) {

            $huoban_obj = new Huoban($huoban_config);
            Cache::put($ticket_name, $huoban_obj['ticket'], 60 * 60 * 24); //最大过期时间1209600

            return $huoban_obj;
        } else {

            $huoban_config['ticket'] = Cache::get($ticket_name);
            $huoban_obj              = new Huoban($huoban_config);

            return $huoban_obj;
        }

    }
}
