<?php

return function (\FastRoute\RouteCollector $route) {
    $route->addRoute('GET', '/api', \App\Http\Controller\IndexController::class);
    $route->addRoute('POST', '/api/index/{action}', \App\Http\Controller\IndexController::class);
    $route->addRoute('POST', '/api/ready/{action}', \App\Http\Controller\ReadyController::class);
    $route->addRoute('POST', '/api/tools/{action}', \App\Http\Controller\ToolsController::class);
};
